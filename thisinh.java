package danhsachquanlithisinh;
public class thisinh {
	public String id;
	public String name;
	public String address;
	public int level;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public int getLevel() {
		return level;
	}
	public void setLevel(int level) {
		this.level = level;
	}

	public thisinh (String id, String name, String address, int level) {
		this.setId(id);
		this.setName(name);
		this.setAddress(address);
		this.setLevel(level);
	}
	public void showInfo() {
		 System.out.println("Thi sinh info:" );
		 System.out.println("-ID\t\t :" + this.getId());
		 System.out.println("-Name\t\t: " + this.getName());
		 System.out.println("-Address\t\t: " + this.getAddress());
		 System.out.println("-Level\t\t: " + this.getLevel());
	 }
}